#!/usr/bin/env python
#imports
import ROOT
import argparse


def calibT(volt):
	A=1444.8355
	B=-1484.1775
	C=363.1961
	T=C*(volt)*(volt) + B*(volt) + A
	return T
	
def calibT2(R):
	if R > 10000:
		T=((-24536.24)+(0.023508289*R*100)+(0.000000001034084*((R*100)*(R*100))))/100 
	else:
		T=((-24564.58)+(0.02353718*R*100)+(0.000000001027502*((R*100)*(R*100))))/100
	return T
	
#-------------MULTIGRAPH FOR EVERY CHANNEL-------------------
mg1 = ROOT.TMultiGraph()
mg1.SetTitle(";Temperature Read [C];ELMB voltage read [V]")
data1={}
#-------------------HISTOGRAM--------------------------------
h1=ROOT.TH1F("h1",";PIXELS VOLTAGE [V]",100,1.52,1.54)
h2=ROOT.TH1F("h2",";STRIPS VOLTAGE [V]",100,1.52,1.54)
h3=ROOT.TH1F("h3",";PIXELS TEMPERATURE [C]",100,20,25)
h4=ROOT.TH1F("h4",";STRIPS TEMPERATURE [C]",100,20,25)
h5=ROOT.TH1F("h5",";PIXELS TEMPERATURE [C]",100,20,25)
h6=ROOT.TH1F("h6",";STRIPS TEMPERATURE [C]",100,20,25)

#-------------------------------data file---------------------------
parser = argparse.ArgumentParser()
parser.add_argument("-d","--input",required=True)#data file read
args = parser.parse_args()

#---------------------data collector-------------------------

file=open(args.input,"r")

for line in file.readlines():

	print(line)
	data = line.strip().split(',')#split by commas line read
	chn= data[1]
	ch=int(chn[2:4])
	location=data[2]
	voltage=float(data[3])/1000000
	Rpt=10*0.799/(voltage-0.799)
	temperature=calibT(voltage)#float(data[4])
	temperature2=calibT2(1000*Rpt)
#--------set data multigraphs

	if not ch in data1: 
		gr=ROOT.TGraph()
		gr.SetMarkerStyle(20)
		data1[ch]=gr
		pass

	data1[ch].AddPoint(voltage,temperature)
	
	if ch==6:
		h2.Fill(voltage)
		h4.Fill(temperature)
		h6.Fill(temperature2)
		
	if ch==17:
		h1.Fill(voltage)
		h3.Fill(temperature)
		h5.Fill(temperature2)
pass




#-----------------------------DATA IN MULTIGRAPH-----------------

	
c1=ROOT.TCanvas("c2","c2",1600,1600)
c1.Divide(2,3)
c1.cd(1)
h1.Draw()
c1.cd(2)
h2.Draw()
c1.cd(3)
h3.Draw()
c1.cd(4)
h4.Draw()
c1.cd(5)
h5.Draw()
c1.cd(6)
h6.Draw()
c1.cd(7)
c1.Update()
c1.SaveAs("c2.pdf")
