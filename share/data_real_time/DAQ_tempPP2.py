#!/usr/bin/env python

import elmb_opc_client
import argparse
import time
import signal
import datetime
import ROOT

def calibT(volt):
    A=1444.8355
    B=-1484.1775
    C=363.1961
    T=C*volt*volt + B*volt + A
    return T

def signal_handler(signal, frame):
    print('You pressed ctrl+C')
    global cont
    cont = False
    return

#This code measure 2 channel ch6_pin13-25_strips (down) and ch17_pin2-14_pixels (up)
parser=argparse.ArgumentParser()
#output file setting
parser.add_argument("-o","--output",required=True)#output file
args = parser.parse_args()


fw=open(args.output,"w")

#client_elmb
client=elmb_opc_client.elmb_opc_client("opc.tcp://pcatlidiot01:48012")
client.Open()

cont = True
signal.signal(signal.SIGINT, signal_handler)

chans=[6,17]
cols={6:ROOT.kRed,17:ROOT.kBlue}

ROOT.gROOT.SetStyle("ATLAS")
mg=ROOT.TMultiGraph()
mg.SetNameTitle("mg",";Time [s];Temperature [C]")
gv={}
for chn in chans:
    gv[chn]=ROOT.TGraph()
    gv[chn].SetNameTitle("g%i"%chn,";Time [s];Temperature [C]")
    gv[chn].SetMarkerStyle(ROOT.kFullCircle)
    gv[chn].SetMarkerColor(ROOT.kRed)
    mg.Add(gv[chn])
    pass
gv[17].SetMarkerColor(ROOT.kBlue)
c1=ROOT.TCanvas("DAQ temperature","",1600,800)

#temperature
#label file 
ss="Date,channel,voltage[uV],temperature[C],channel,voltage[uV],temperature[C]"
fw.write("%s\n" % ss)
#data read
t0=datetime.datetime.now()
while cont:
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    #data write
    ss=timestamp    
    dt=datetime.datetime.now()-t0
    for chn in chans:
        schn="ch%i"%chn
        volt=0
        temp=0
        try:
            volt=client.nodes["bus1"]["elmb1"]["TPDO3"][schn].get_value()
            temp=calibT(volt/1e6)
            gv[chn].AddPoint(dt.total_seconds(),temp)
            pass
        except:
            print("Could not read data from chn: %i" % chn)
            pass
        ss+=",%s,%i,%.04f" % (chn,volt,temp)
        pass
    print(ss)
    fw.write("%s\n" % ss)
    mg.Draw("AP")
    c1.Update()
    ROOT.gSystem.ProcessEvents()
    time.sleep(10)
    pass

print("Close the connection")
client.Close()
print("Have a nice day")
