#!/usr/bin/env python
#imports
import argparse
import HAMEG4040
import time
import SerialCom
import argparse
import elmb_opc_client
import datetime
#Run code
#python DAQ_totalBoard_tempPP2.py -r /dev/ttyACM0 -a /dev/ttyACM1 -o 2024.06.14_DAQ_report.txt


#FUNCTIONS
def calibT(volt):
    A=1444.8355
    B=-1484.1775
    C=363.1961
    T=C*volt*volt + B*volt + A
    return T

def signal_handler(signal, frame):
    print('You pressed ctrl+C')
    global cont
    cont = False
    return
    
#CLIENT ELMB
client=elmb_opc_client.elmb_opc_client("opc.tcp://pcatlidiot01:48012")
client.Open()

#VARIABLES
increase = 0.13
#varaiables with initial states
Vin = 3.2 #2
forward= True
up = True
ch=0
#Channel Map
MuxMap=["4","5","8","9","12","13","14","3","6","7","10","11","27","26","23","22","19","18","17","28","25","24","21","20","4","5","8","9","12","13","14","3","6","7","10","11","27","26","23","22","19","18","17","28","25","24","21","20"]
#Key for the Map [0 to 24] every channel
Current_Ch=0
#communication 
parser = argparse.ArgumentParser()
parser.add_argument("-r","--rohde",required=True)# VAP -VAP Vin
parser.add_argument("-a","--arduino",required=True)# arduino
parser.add_argument("-o","--output",required=True)#output file

args = parser.parse_args()

ardui = SerialCom.SerialCom(args.arduino,9600,timeout=1)#arduino
hameg = HAMEG4040.HAMEG4040(args.rohde)# VAP -VAP Vin

#GRAPHS

#MAIN PROGRAM

# 1. Enable Power supplies and data report file
print("enable power to SCB")
hameg.setVoltage(3,13.5)
hameg.enableOutput(3,True)
hameg.setVoltage(4,13.5)
hameg.enableOutput(4,True)
hameg.setVoltage(2,Vin)
hameg.enableOutput(2,True)
fw=open(args.output,"w")

#----------------------------------channel change--------------------------------------------
for rep in range(24):
	# 3. Read Channel
	forward = True
	ret=ardui.writeAndRead(MuxMap[Current_Ch])
	time.sleep(4)	
	print("::%s" % ret)
#----------------------------------channel sweep----------------------------------------------
	while forward:
		# checks if Vin is in the temperature spectrum
		if (Vin <= 7.6) and (Vin >=3.2):
			hameg.setVoltage(2,Vin)
			print(Vin,"\n")
#----------------------------------current value----------------------------------------------		
			for rept in range(10):
				time.sleep(5)
				#channel
				ch = int(MuxMap[Current_Ch])
				
				#elmb voltage
				schn="ch%i"%Current_Ch
				volt=client.nodes["bus1"]["elmb1"]["TPDO3"][schn].get_value()
				
				#elmb temperature
				temp=calibT(volt/1e6)
		    		
				#date 
				timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		    		
				#get string
				ss=timestamp
				ss+=",%s,%.3f,%.3f" % (schn,volt/1e6,temp)
				print(ss)
				
				#print string
				fw.write("%s\n" % ss)
				pass
#----------------------------------voltage imput setting ----------------------------------------
			if up:
				Vin+=increase
			else:
				Vin-=increase
			
		else: 
			Current_Ch+=1
			up=not up
			forward = False
			if up:
				Vin+=increase
			else:
				Vin-=increase
			pass
		pass
		
pass
