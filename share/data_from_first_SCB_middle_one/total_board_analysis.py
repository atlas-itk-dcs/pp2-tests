#!/usr/bin/env python
#imports
import ROOT
import numpy as np
from datetime import datetime
#Run code
#python -i 2024.05.17_analysis_SCB1.5_total_board.py  

#---------------------------------------------------FUNCTIONS------------------------------------------------------------------

#Temperature function
def Temperature(R_SCB):
	A=3.9083E-3
	B=-5.775E-7
	C=-4.183E-12
	R0=10000
	if R_SCB < R0:
		# 0 = (C4)T^4 + (C3)T^3 + (C2)T^2 + (C1)T + (C0)
		C4 = C*R0
		C3 = -C*R0*100
		C2 = B*R0
		C1 = A*R0 
		C0 = R0-R_SCB
		Tempf = ROOT.Math.Polynomial(C4,C3,C2,C1,C0)
	else:
		# 0 = (C2)T^2 + (C1)T + (C0)
		C2 = B*R0
		C1 = A*R0
		C0 = R0-R_SCB
		Tempf = ROOT.Math.Polynomial(C2,C1,C0)
	
	return Tempf
	
pass


#---------------------------------------------------VARIABLES-----------------------------------------------------------------

#MEASUREMENTS
Ch = 0 # channel read
Iout = 0 # source current from the VRB in uA
Vd = 0 # experimental Vref measured
Vout = 0 # output voltage from SCB
Vin = 0 # input voltage from main power supply
Vref=0 # theoretical Vref then is set 
points=0 #points taken
#FLAG VARIABLES
current_Vin=0 # when Vin change  
sample_counter=0 # samples registered
flag_data = False # flag in false

#error
maximum_error=0 #Maximum delta measured collector
current_e1=0#maximum error from VRB
current_e2=0#maximum error from SCB
channel_error1=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]#channel error map
channel_error2=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]#channel error map

#--------------------------------------------------STYLE IMPORT--------------------------------------------------------- 
										
#ATLAS style
ROOT.gROOT.SetStyle("ATLAS")
legend = ROOT.TLegend(.85,.60,.95,.95)
#Colors for every channel
color_map=[1,11,3,4,5,6,7,8,9,30,38,40,41,42,43,44,45,47,28,23,21,12,33,16]
MuxMap=[4,5,8,9,12,13,14,3,6,7,10,11,27,26,23,22,19,18,17,28,25,24,21,20]

#---------------------------------------------------GRAPHS---------------------------------------------------------------- 
											
#multigraph Graph format
mg1 = ROOT.TMultiGraph()# Voltage (SCB) VS Temperature (VRB)

#multigraph names
mg1.SetTitle(";Voltage read from ELMB [V];Temperature [C]")#  Temperature (VRB) VS Voltage (SCB)

#data management
data1={}#temp (VRB)


#tf to fit  
tf1={}# Voltage (SCB) VS Temperature (VRB)
tf2={}# Voltage (SCB)VS Temperature (SCB)

#-------------------------------------------------HISTOGRAMS-----------------------------------------------------------------
										
#analysis Voltage (SCB) VS Temperature (VRB) and Voltage (SCB)VS Temperature (SCB) parameters
h1=ROOT.TH1F("h1",";P0_VRB [C]",100,1000,2000)
h2=ROOT.TH1F("h2",";P1_VRB [C/V]",100,-2000,-1000)
h3=ROOT.TH1F("h3",";P2_VRB [C/V^2]",100,300,700)
h4=ROOT.TH1F("h4",";P0_SCB [C]",100,1000,2000)
h5=ROOT.TH1F("h5",";P1_SCB [C/V]",100,-2000,-1000)
h6=ROOT.TH1F("h6",";P2_SCB [C/V^2]",100,300,700)

#Error Delta Temperature (VRB-SCB)
h9=ROOT.TH1F("Delta_Temperature",";#Delta Temperature [C]",50,-1,1)

#Vref
h10=ROOT.TH1F("Vref_histogram",";Vref [V]",10,0.7,0.9) #Vref

#------------------------------------------------------MAIN----------------------------------------------------------


#--------data collector

file=open('2024.06.14_DAQ_report.txt')#open the file

for line in file.readlines():

	data = line.strip().split(',')#split by commas line read
	chn= data[1]
	ch = int(chn[2:4])
	Vout = float(data[2])
	Temp = float(data[3])

					
						
#--------set data multigraphs

	if not ch in data1: 
		gr=ROOT.TGraph()
		gr.SetMarkerStyle(20)
		gr.SetMarkerColor(color_map[ch])
		legend.AddEntry(gr,"channel: %5i"%(ch))
		data1[ch]=gr
		pass


#----------save data from -40 to 25 degrees celsius 
			
	if (Temp > -45) and (Temp < 25):
		
		#output response
		data1[ch].AddPoint(Vout,Temp) # Temperature (VRB) VS Voltage (SCB)
		
			
pass

#---------------------------------------------------FILL AND FIT DATA-------------------------------------------------- 
for ch in data1:
	mg1.Add(data1[ch])
	tf1[ch] = ROOT.TF1('tf1_1_%i'%ch,'pol2')
	data1[ch].Fit(tf1[ch],"","",1,2)
	data1[ch].GetFunction('tf1_1_%i'%ch).SetLineColor(color_map[ch])
	pass


#--------------------------------------------PLOT AND EXPORT DATA------------------------------------------------------------

#------------------DATA 1------------------
#draw the Graph
c1=ROOT.TCanvas("c1","c1",1600,1600) 

#first paths
c1.cd(1)
mg1.Draw("AP")
legend.Draw()
mg1.GetXaxis().SetRangeUser(1,2.5)
mg1.GetYaxis().SetRangeUser(-45,25)



#print the draw
c1.Update()
c1.SaveAs("C1.pdf")		

#------------HISTOGRAMS FROM PARAMETERS----
for ch in sorted(tf1):
	h1.Fill(tf1[ch].GetParameter(0))
	h2.Fill(tf1[ch].GetParameter(1))
	h3.Fill(tf1[ch].GetParameter(2))
	pass
	
c2=ROOT.TCanvas("c2","c2",1600,1600)
c2.Divide(3,1)
c2.cd(1)
h1.Draw()
c2.cd(2)
h2.Draw()
c2.cd(3)
h3.Draw()

#print the draw
c2.Update()
c2.SaveAs("c2.pdf")

#---------------------------------------Final report-----------------------------------------
#write the output transfer function
fw=open("Transfer_function_report.txt","w")
ss = "A(MEAN),A(STDV),B(MEAN),B(STDV),C(MEAN),C(STDV)\n" 
fw.write(ss)

#----transfer function coefficients---------------
ss="%10.4f,%10.4f,"  % (h1.GetMean(1),h1.GetStdDev(1)) # a mean and a stdv
ss+="%10.4f,%10.4f," % (h2.GetMean(1),h2.GetStdDev(1))# b mean and b stdv
ss+="%10.4f,%10.4f" % (h3.GetMean(1),h3.GetStdDev(1))# c mean and c stdv
fw.write(ss)
fw.close()
